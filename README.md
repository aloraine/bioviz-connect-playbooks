## Playbooks to set up a Bioviz Connect site

This repository contains ansible playbooks for setting up and updating
`bioviz-connect` instances.

See `bioviz-connect` repositories: 

* The upstream repository: https://bitbucket.org/nfreese/bioviz-connect 
* Loraine Lab fork: https://bitbucket.org/lorainelab/bioviz-connect

There are two important playbooks to understand and use. 

### Playbook `ec2.yml` (run once per host)

The first playbook `ec2.yml` does the following:

* creates an ubuntu ec2 virtual machine for hosting `bioviz-connect` in your AWS account
* saves its keypair to your local .ssh folder (so that you can connect to it later)
* assigns it a static (elastic) IP address
* creates and attaches a security group (firewall) to the ec2
* configures the security group

Note that the security group is configured to allow the machine
running `ansible-playbook` to connect to the ec2. So if you are
running ansible on your personal computer, the security group will be
configured to enable your computer to connect to the ec2 host and run
further commands.

Also, you do not have to host `bioviz-connect` instances on an
ec2. You could use a different machine if you like, provided it is an
ubuntu Linux machine. Similarly, once the host is created, you do not
need to run the `ec2.yml` playbook again.

To run the `ec2.yml` playbook, you must install the following on the
machine where you run ansible:

* python3
* ansible

and resources needed to communicate with AWS:

* boto 
* boto3
* botocore

Also, you must create a variables file `group_vars/ec2_vars.yml` which
contains AWS credentials for your account and a few other things. See
`group_vars/example_ec2_vars.yml`.

### Playbook `setup.yml` (run many times)

The second playbook is `setup.yml`, which does a lot more, including:

* installs and configures software onto the host 
* clones a bioviz-connect repository (you designate which one, or use the default)
* connects to CyVerse to retrieve information about bioviz-connect apps hosted on CyVerse

Run this second playbook `setup.yml` whenever you need to update your
`bioviz-connect` instance with new code.  To run `setup.yml`, you need
to install python3 and ansible onto your machine. However, you do
**not** need any AWS credentials or anything AWS-related. The only
special credentials you will need are:

* a private key for `ansible-playbook` to connect to the `bioviz-connect` target host machine
* certificate files for your domain to enable https connections 

To run `setup.yml`, do the following:

* install python3 and ansible onto your computer 
* obtain the private key file needed to connect to the target host where you will install and run `bioviz-connect`
* create an inventory file listing the host's IP address, the private key needed to access it, its domain name, and `secret_key` value to identify the django web application (this can be anything); see `example_inventory.ini` 
* create `group_vars/setup_vars.yml`; see `group_vars/example_setup_vars.yml`

Note that bioviz-connect will only allow connections via https. This
means you will need to obtain certificate files for your
bioviz-connect host's domain name. Also, this domain name will need to
match the client id which your bioviz-connect site will use to connect
to CyVerse resources. If you have any questions about this, contact
us.

Once you done the above, run the playbook:

`ansible-playbook -i [your inventory yml file] setup.yml`

Note that you can specify the repository and branch name for the
`bioviz-connect` code you wish to deploy. Use this feature to test new
code in a close-to-production environment.

### Check it's working

Try to visit the domain using your Web browser. If the domain
indicated in the inventory file is not associated with the actual IP
address of your host, that's fine. Just modify your computer's `hosts`
file to associate the domain with the IP address. Later, you can
register it in DNS.

When you visit the domain, your Web browser will get redirected to the
CyVerse login page. Log in with your CyVerse user name and password.

Next, the CyVerse login page will redirect your Web browser back to
your BioViz-Connect instance. At that point, you should see a page
showing your CyVerse home directory.

If instead you see a blank page, there could be two things going wrong:

* Option one: Your IP address is not permitted to connect to CyVerse endpoints. 

To check if this is the case, log into your Bioviz-Connect host. Check that you can access CyVerse Discovery Environment Terrain API using `curl`:

`curl https://de.cyverse.org/terrain`

which should print:

`The infinite is attainable with Terrain!`

If the `curl` command times out, it means your host's IP address is
blocked. If this happens, contact Nowlan Freese (see below) for
assistance in getting your IP address unblocked.

* Option two: Your redis-server software is misconfigured and data returned from CyVerse Terrain API is not getting properly saved.

Check first that redis-server is actually running. Log in to your host, change to the root user, and execute:

`redis-cli`

This should print the redis command prompt:

`127.0.0.1:6379> `

Enter the following and observe what gets printed. 

`127.0.0.1:6379> select 1`

which should print `OK`.

Next, check to see if data has been saved to the redis datastore:

`127.0.0.1:6379[1]> keys *`


The `keys *` command will print all keys saved in the current
instance. If bioviz-connect is properly configured, then logging in to
CyVerse via bioviz-connect will cause some data to be saved in the
redis-server's in-memory database. If no keys are printed, there may
be a problem with permissions. The Web user `www-data` needs to be
able to interact with the socket file when redis starts. Contact Ann
Loraine for help in that case.

Lastly, make sure that analysis apps for IGB are properly configured.
Locate a "bam" file in your account, or use the Bioviz-Connect interface to 
transfer a test "bam" file into your account.

Once it is there, right-click the file name. You should see a new panel with
some options for apps that can accept a 'bam' file as input. If you see 
nothing, that means the `sync_app` scripts has failed for some reason. This
can also be due to cyverse endpoints not permitting your host to connect.
Contact Nowlan Freese for help with that.

#### Contacts

* Ann Loraine (playbooks developer)
* Nowlan Freese - nfreese@uncc.edu (bioviz-connect developer)
* Karthik Raveendran (bioviz-connect developer)